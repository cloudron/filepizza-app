FilePizza enables fast and private peer-to-peer file transfers in your web browser.

### About

Using [WebRTC](http://www.webrtc.org), FilePizza eliminates the initial upload step
required by other web-based file sharing services. When senders initialize a transfer,
they receive a "tempalink" they can distribute to recipients. Upon visiting this link,
recipients' browsers connect directly to the sender’s browser and may begin downloading
the selected file. Because data is never stored in an intermediary server, the transfer
is fast, private, and secure.

## FAQ

**Where are my files sent?** Your files never touch our server. Instead, they are sent
directly from the uploader's browser to the downloader's browser using WebTorrent and WebRTC.
This requires that the uploader leave their browser window open until the transfer is complete.

**Can multiple people download my file at once?** Yes! Just send them your tempalink.

**How big can my files be?** Chrome has issues supporting files >500 MB. Firefox does not have
any issues with large files, however.

**What happens when I close my browser?** The tempalink is invalidated. If a downloader has
completed the transfer, that downloader will continue to seed to incomplete downloaders, but no
new downloads may be initiated.

**Are my files encrypted?** Yes, all WebRTC communications are automatically encrypted using
public-key cryptography.

**My files are sending slowly!** Transfer speed is dependent on your network connection.

