# File Pizza Cloudron App

This repository contains the Cloudron app package source for [FilePizza](http://file.pizza/).

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=pizza.file.cloudronapp)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id pizza.file.cloudronapp
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd filepizza-app

cloudron build
cloudron install
```

