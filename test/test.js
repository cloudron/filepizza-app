#!/usr/bin/env node

/* jshint esversion: 6 */
/* global it:false */
/* global xit:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;
    const TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 20000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    const LOCATION = 'test';
    let browser, app;

    before(function () {
        browser = new Builder().forBrowser('chrome').setChromeOptions(new Options().windowSize({ width: 1280, height: 1024 })).build();
    });

    after(function () {
        browser.quit();
    });

    async function checkWebsite() {
        await browser.get('https://' + app.fqdn);
        await browser.wait(until.elementLocated(By.xpath('//span[contains(text(), "select a file")]')), TIMEOUT);
    }

    async function login() {
        await browser.get(`https://${app.fqdn}/login`);
        await browser.wait(until.elementLocated(By.xpath('//input[@name="username"]')), TIMEOUT);
        await browser.findElement(By.xpath('//input[@name="username"]')).sendKeys(username);
        await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(password);
        await browser.findElement(By.css('form')).submit();
        await browser.wait(until.elementLocated(By.xpath('//span[contains(text(), "select a file")]')), TIMEOUT);
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}/logout`);
        await browser.wait(until.elementLocated(By.xpath('//input[@name="username"]')), TIMEOUT);
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.startsWith(LOCATION); })[0];
        expect(app).to.be.an('object');
    }

    xit('build app', function () {
        execSync('cloudron build', EXEC_ARGS);
    });

    it('install app', function () {
        execSync('cloudron install --no-sso --location ' + LOCATION, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);

    it('can view website', checkWebsite);

    it('can restart app', function () {
        execSync('cloudron restart');
    });

    it('can view website', checkWebsite);

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, EXEC_ARGS);
    });

    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw'));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --no-sso --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can view website', checkWebsite);

    it('move to different location', async function () {
        await browser.manage().deleteAllCookies();
        execSync('cloudron configure --location ' + LOCATION + '2', EXEC_ARGS);
        getAppInfo();
    });

    it('can view website', checkWebsite);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    it('install app (sso)', function () {
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);

    it('can login', login);
    it('can view website', checkWebsite);
    it('can logout', logout);
    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // test update
    it('can install app', function () {
        execSync('cloudron install --no-sso --appstore-id pizza.file.cloudronapp --location ' + LOCATION, EXEC_ARGS);
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');
    });

    it('can update', function () {
        execSync('cloudron update --app ' + LOCATION, EXEC_ARGS);
        getAppInfo();
    });

    it('can view website', checkWebsite);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });
});
