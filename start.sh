#!/bin/bash

set -eu

export ICE_SERVERS="[{ \"urls\": \"stun:$CLOUDRON_STUN_SERVER:$CLOUDRON_STUN_PORT\" }]"

echo "==> Starting FilePizza"
node dist/index.js
