FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

ARG NODE_VERSION=12.22.12
RUN mkdir -p /usr/local/node-${NODE_VERSION} && \
    curl -L https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.gz | tar zxf - --strip-components 1 -C /usr/local/node-${NODE_VERSION}
ENV PATH /usr/local/node-${NODE_VERSION}/bin:$PATH

RUN mkdir -p /app/code
WORKDIR /app/code
RUN curl -L https://github.com/kern/filepizza/tarball/e4fb3431fb5f3b3e5593e44df3837017ffc10f86 | tar -xz --strip-components 1 -f -
RUN npm install && npm run-script build

USER cloudron

COPY start.sh /app/code/start.sh

CMD [ "/app/code/start.sh" ]
